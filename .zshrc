# _____   ____  _   _ _____  ____
# ` / /_ (_ (_`| |_| || () )/ (__`
#  /___/.__)__)|_| |_||_|\_\\____)

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=100000
bindkey -e
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/mario/.zshrc'
autoload -Uz compinit
compinit
# End of lines added by compinstall
# allow comments in interactive shells
setopt INTERACTIVE_COMMENTS
# aliases
alias cal='cal -mw'
alias cat='bat'
alias dt='dutree -da'
alias du='du -sh'
alias fd='fd -H'
alias gc='git clone'
alias genpw_16="cat /dev/urandom | tr -dc 'a-zA-Z0-9!#$%&=+~*' | fold -w 16 | head -n 1"
alias genpw_24="cat /dev/urandom | tr -dc 'a-zA-Z0-9!#$%&=+~*' | fold -w 24 | head -n 1"
alias hm='himalaya'
alias ls='eza --group-directories-first --sort=name --icons'
# alias ls='ls --color=auto --group-directories-first'
alias la='ls -a'
alias ll='ls -la'
alias lt='ls -T -L 4'
alias mt='udisksctl mount -b'
alias ut='udisksctl unmount -b'
alias nh='nvim ~/.histfile'
alias nc='nnn -d ./'
alias nn='nnn -d /'
alias nz='nvim ~/.zshrc'
alias python='python3.13'
alias us='setxkbmap us -option compose:ralt'
alias zc="zstd -c -T0 --ultra -20 -o"
alias zi="zstd -l"
alias zu="tar -I zstd -xvf"
# shell extensions
# source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
# exports
export VISUAL=nvim
export EDITOR="$VISUAL"
export PATH=$PATH:$HOME/.local/bin:$HOME/.cargo/bin
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
# autostart X from tty1
if [ "$(tty)" = "/dev/tty1" ]; then startx; fi
# deprecated: revert font size in tty:
# if [ "$TERM" = "linux" ]; then setfont -v; fi
# deprecated: set prompt for graphical terminal:
# if [ "$TERM" != "linux" ]; then PS1='%F{bwhite}%B--> %b'; fi
source ~/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

export NNN_TMPFILE="/tmp/nnn"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
