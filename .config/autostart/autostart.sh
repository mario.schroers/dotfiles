#! /bin/zsh
# dwm autostart script as launched per ~/.xinitrc
# dwm clock
while true; do xsetroot -name "$( date +"%F %R" )"; sleep 1s; done &
# set wallpaper
feh --bg-fill --no-xinerama ~/Pictures/3440_1440_043.jpg
# launch picom
picom &
