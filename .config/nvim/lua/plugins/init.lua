-- Packer Configuration File
return require('packer').startup(function()
    use 'wbthomason/packer.nvim'
    use 'nvim-lualine/lualine.nvim'
    use 'shaunsingh/nord.nvim'
    vim.g.nord_borders = true
    use 'Shougo/deoplete.nvim'
    vim.g['deoplete#enable_at_startup'] = 1
end)
