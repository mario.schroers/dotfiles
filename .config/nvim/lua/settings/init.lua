local set = vim.opt
set.autoindent = true
set.clipboard = 'unnamedplus'
set.conceallevel = 2
set.cursorcolumn = false
set.cursorline = true
set.expandtab = true
set.fileencoding = 'utf-8'
set.hidden = true
set.hidden = true
set.history = 100
set.hlsearch = true
set.ignorecase = true
set.incsearch = true
set.laststatus = 3
set.lazyredraw = true
set.linebreak = true
set.list = false
set.listchars = { space = '·', extends = '>', nbsp = '°',  precedes = '<', space = '·', tab = '>-', trail = '~' }
set.number = true
set.relativenumber = true
set.scrolloff = 4
set.shiftwidth = 4
set.showmode = false
set.smartcase = true
set.smartindent = true
set.smarttab = true
set.splitbelow = true
set.splitright = true
set.tabstop = 4
set.termguicolors = false
set.wrap = true
vim.api.nvim_set_hl(0, 'Comment', { italic=true })
vim.api.nvim_set_hl(0,'CursorLine',{ bg='#4c566a' })
vim.g.python3_host_prog = '/usr/bin/python3'
vim.cmd[[colorscheme nord]]
require('lualine').setup()
